<?php

namespace AppBundle\Service;

use Predis;
use Predis\Connection\ConnectionException;
use Symfony\Bridge\Monolog\Logger;

/**
* Here you have to implement a CacheService with the operations above.
* It should contain a failover, which means that if you cannot retrieve
* data you have to hit the Database.
**/
class CacheService
{
    private $client;

    /**
     * CacheService constructor.
     *
     * @param string          $host
     * @param string          $port
     * @param string          $prefix
     */
    public function __construct($host, $port, $prefix = null)
    {
        try {
            $this->client = new Predis\Client("tcp://$host:$port", ['prefix' => $prefix]);
            @$this->client->connect();
        } catch (ConnectionException $connException) {
            # Whatever the logic for such error is
        }

    }

    /**
     * @param string $key
     * @return string
     */
    public function get($key)
    {
        return unserialize($this->client->get($key));
    }

    /**
     * @param string $pattern
     * @return array
     */
    public function getAll($pattern)
    {
        if ($this->client->isConnected() === false) {
            return [];
        }

        $keys = $this->client->keys($pattern . '*');
        $records = array();

        foreach ($keys as $key) {
            $records[] = $this->get($key);
        }

        return $records;
    }

    /**
     * @param string $key
     * @param object $value
     *
     * @return bool
     */
    public function set($key, $value)
    {
        $this->client->set($key, serialize($value));
        return true;
    }

    /**
     * @param string            $key
     * @param array|Traversable $values
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    public function setMany($key, $values)
    {
        if (!is_array($values) && !($values instanceof \Traversable)) {
            throw new \InvalidArgumentException("Second argument should be an array or an instance of Traversable");
        }

        foreach ($values as $value) {
            $this->set($key . $value['_id'], $value);
        }

        return true;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function del($key)
    {
        $this->client->del($key);
        return true;
    }

    /**
     * @param string $pattern
     * @return bool
     */
    public function delAll($pattern)
    {
        $keys = $this->client->keys($pattern . '*');
        foreach ($keys as $key) {
            $this->del($key);
        }

        return true;
    }
}
