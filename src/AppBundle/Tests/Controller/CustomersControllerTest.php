<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CustomersControllerTest extends WebTestCase
{
    protected $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->client->followRedirects();
    }

    public function testCreateCustomers()
    {
        $customers = [
            ['name' => 'Leandro', 'age' => 26],
            ['name' => 'Marcelo', 'age' => 30],
            ['name' => 'Alex', 'age' => 18],
        ];
        $customers = json_encode($customers);

        $this->client->request('POST', '/customers/', [], [], ['CONTENT_TYPE' => 'application/json'], $customers);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function testCannotCreateEmptyCustomers()
    {
        $customers = [];
        $customers = json_encode($customers);

        $this->client->request('POST', '/customers/', [], [], ['CONTENT_TYPE' => 'application/json'], $customers);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertFalse($this->client->getResponse()->isSuccessful());
    }

    public function testCannotCreateNullCustomers()
    {
        $customers = null;
        $customers = json_encode($customers);

        $this->client->request('POST', '/customers/', [], [], ['CONTENT_TYPE' => 'application/json'], $customers);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertFalse($this->client->getResponse()->isSuccessful());
    }

    /**
     * @depends testCreateCustomers
     */
    public function testGetCustomersAction()
    {
        $this->client->request('GET', '/customers/');

        $content = json_decode($this->client->getResponse()->getContent());
        
        $this->assertCount(3, (array)$content);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @depends testCreateCustomers
     */
    public function testDeleteCustomersAction()
    {
        $this->client->request('DELETE', '/customers/');
        $this->assertEquals($this->client->getResponse()->getStatusCode(),200);
    }
}
