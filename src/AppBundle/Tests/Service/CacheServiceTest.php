<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\CacheService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CacheServiceTest extends WebTestCase
{
    /** @var CacheService $cache */
    protected $cache;

    public function setUp()
    {
        $this->cache = new CacheService('127.0.0.1', 6379);
    }

    public function testSetAndGetOneKey()
    {
        $this->cache->set('setOneTest', 'Setting one Value');

        $this->assertEquals('Setting one Value', $this->cache->get('setOneTest'));
    }

    public function testSetAndGetManyKeys()
    {
        $this->cache->setMany('manyRecords', [
            ['_id' => 001, 'value' => 'ValueOne'],
            ['_id' => 002, 'value' => 'ValueTwo'],
            ['_id' => 003, 'value' => 'ValueThree']
        ]);

        $this->assertCount(3, $this->cache->getAll('manyRecords'));
    }

    /**
     * @depends testSetAndGetOneKey
     */
    public function testDeleteOneKey()
    {
        $this->cache->del('setOneTest');
        $this->assertFalse($this->cache->get('setOneTest'));
    }

    /**
     * @depends testSetAndGetManyKeys
     */
    public function testDeleteAll()
    {
        $this->cache->delAll('manyRecords');
        $this->assertCount(0, $this->cache->getAll('manyRecords'));
    }
}